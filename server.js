const express=require('express');
const bodyParser=require('body-parser');
const Sequelize=require('sequelize');
const path = require("path");

const app=express();
app.use(bodyParser.json());

const sequelize= new Sequelize('gpsdb','ancamariapopescu','',{host:'localhost', dialect: 'mysql',define : {
		timestamps : false
	}});

sequelize.authenticate().then(()=>
{
    console.log('Database connection success!');
    
}).catch((err)=>{
    console.log(`Database connection error${err}`);
    
});

const User=sequelize.define('user', {
    email:{
        type:Sequelize.STRING,
        isUnique:true,
        allowNull:false,
        validate: {
	        isEmail : true
	    }
    },
    first_name:{
        type:Sequelize.STRING,
        allowNull:false,
        validate: {
            is: ["^[a-z]+$",'i'], //will only allow letters
            len: [3,20] //only allow values with length between 3 and 20
        }
    },
    last_name:{
        type:Sequelize.STRING,
        allowNull:false,
        validate: {
            is: ["^[a-z]+$",'i'],
            len: [3,20]
        }
    },
    password:{
        type:Sequelize.STRING,
        allowNull:false,
        validate: {
            len: [8,12]
        }
    },
    user_type:{
        type:Sequelize.STRING,
        allowNull:false
    },
    company:{
        type:Sequelize.STRING,
        allowNull:true
        
    },
    phone_number:{
        type:Sequelize.STRING,
        allowNull:false,
        isUnique:true,
        validate:{
            isNumeric: true, // will only allow numbers
            len:[10]
		
        }
    },
},{
	underscored : true //foreignKey will be snake_case
});

const Field=sequelize.define('field', {
    name:{
        type:Sequelize.STRING,
        allowNull:false,
        defaultValue : "Other"
    }
},{
	underscored : true
});

const Tweet =sequelize.define('tweet', {
    posting_date:{
        type:Sequelize.DATE,
        allowNull:false
    },
    content:{
        type:Sequelize.STRING,
        allowNull:false
    }
    
},{
	underscored : true
});

const Company=sequelize.define('company', {
    name:{
        type:Sequelize.STRING,
        allowNull:false
    }
},{
	underscored : true
});


User.belongsToMany(Field, {through:'field_items'});//create a new model called field_items with the equivalent foreign keys user_id and field_id
Field.belongsToMany(User, {through:'field_items'});

Field.belongsToMany(Company, {through:'company_items'});
Company.belongsToMany(Field, {through:'company_items'});

Company.hasMany(Tweet);// add company_id to tweets

sequelize.sync().then(()=>{
    console.log('Tables created succed!');
}).catch((err)=>{
    console.log(`Failed to create tables ${err}`);
});


app.get('/', (req, res) => {
    res.sendFile(path.resolve(__dirname, './.', 'frontend/project-app/build', 'index.html'));
});


app.use(express.static(path.resolve(__dirname, './frontend/project-app', 'build')));



//create new user
app.post('/register-user', async(req,res)=>{
    User.create({
        email:req.body.email,
        first_name:req.body.first_name,
        last_name:req.body.last_name,
        password:req.body.password,
        user_type:req.body.user_type,
        company:req.body.company,
        phone_number:req.body.phone_number
    }).then((user)=>{
        res.status(200).send(user);
    }).catch((err)=>{
        res.status(500).send(err);
    });
});

//create new company
app.post('/register-company',async(req,res)=>{
    Company.create({
        name:req.body.name
    }).then((company)=>{
        res.status(200).send(company);
    }).catch((err)=>{
        res.status(500).send(err);
    });
});

//create new field
app.post('/register-field',async(req,res)=>{
    Field.create({
        name:req.body.name
    }).then((field)=>{
        res.status(200).send(field);
    }).catch((err)=>{
        res.status(500).send(err);
    });
});

//create new tweet
app.post('/register-tweet',async(req,res)=>{
	
    Tweet.create({
        posting_date:Date.now(),
        content:req.body.content
    }).then((tweet)=>{
        res.status(200).send(tweet);
    }).catch((err)=>{
        res.status(500).send(err);
    });
});

//show all users
app.get('/get-all-users', async(req,res) =>{
   try{
		let users = await User.findAll()
		res.status(200).json(users)
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
    
});

//show all fields
app.get('/get-all-fields', async(req,res) =>{
   try{
		let fields = await Field.findAll()
		res.status(200).json(fields)
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
    
});

//show all companies
app.get('/get-all-companies', async(req,res) =>{
   try{
		let companies = await Company.findAll()
		res.status(200).json(companies)
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
    
});

//show user by id
app.get('/users/:id',async(req, res) => {
   	try{
		let user = await User.findById(req.params.id)
		if (user){
			res.status(200).json(user)
		}
		else{
			res.status(404).json({message : 'not found'})
		}
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
});

//a field is assigned to a user - an user can have multiple fields
app.post('/users/:id/fields', async (req, res) => {
	try {
		let user = await User.findById(req.params.id)
		if (user){
			let field = await Field.create(req.body)
			user.addField(field)
			res.status(201).json({message : 'created'})
		}
		else{
			res.status(404).json({message : 'not found'})
		}
	} catch (e) {
		console.warn(e.stack)
		res.status(500).json({message : 'server error'})
	}	
})

//show user by id with fields
app.get('/users/:id/fields', async (req, res) => {
	try {
		let user = await User.findById(req.params.id)
		if (user){
			let fields = await user.getFields()
			res.status(200).json(fields)
		}
		else{
			res.status(404).json({message : 'not found'})
		}
	} catch (e) {
		console.warn(e.stack)
		res.status(500).json({message : 'server error'})
	}
})

//a field is assigned to a company - a company can have multiple fields
app.post('/company/:id/fields', async (req, res) => {
	try {
		let company = await Company.findById(req.params.id)
		if (company){
			let field = await Field.create(req.body)
			company.addField(field)
			res.status(201).json({message : 'created'})
		}
		else{
			res.status(404).json({message : 'not found'})
		}
	} catch (e) {
		console.warn(e.stack)
		res.status(500).json({message : 'server error'})
	}	
})

//show company by id with fields
app.get('/company/:id/fields', async (req, res) => {
	try {
		let company = await Company.findById(req.params.id)
		if (company){
			let fields = await company.getFields()
			res.status(200).json(fields)
		}
		else{
			res.status(404).json({message : 'not found'})
		}
	} catch (e) {
		console.warn(e.stack)
		res.status(500).json({message : 'server error'})
	}
})

//a tweet is assigned to a company - a company can have multiple tweets
app.post('/company/:id/tweets', async (req, res) => {
	try {
		let company = await Company.findById(req.params.id)
		if (company){
			let tweet = await Tweet.create({...req.body, posting_date:Date.now()})
			company.addTweet(tweet)
			res.status(201).json({message : 'created'})
		}
		else{
			res.status(404).json({message : 'not found'})
		}
	} catch (e) {
		console.warn(e.stack)
		res.status(500).json({message : 'server error'})
	}	
})

//show company by id with tweets
app.get('/company/:id/tweets', async (req, res) => {
	try {
		let company = await Company.findById(req.params.id)
		if (company){
			let tweets = await company.getTweets()
			res.status(200).json(tweets)
		}
		else{
			res.status(404).json({message : 'not found'})
		}
	} catch (e) {
		console.warn(e.stack)
		res.status(500).json({message : 'server error'})
	}
})

app.get('/company/:id', async (req, res) => {
	try{
		let company = await Company.findById(req.params.id)
		if (company){
			res.status(202).json(company)
		}
		else{
			res.status(404).json({message : 'not found'})
		}
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
})

//update for a company by id
app.put('/company/:id', async (req, res) => {
	try{
		let company = await Company.findById(req.params.id)
		if (company){
			await company.update(req.body)
			res.status(202).json({message : 'accepted'})
		}
		else{
			res.status(404).json({message : 'not found'})
		}
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
})

//delete company by id
app.delete('/company/:id', async (req, res) => {
	try{
		let company = await Company.findById(req.params.id)
		if (company){
			await company.destroy()
			res.status(202).json({message : 'accepted'})
		}
		else{
			res.status(404).json({message : 'not found'})
		}
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
})

app.listen(8080,()=>{
    console.log('Server started on port 8080');
});