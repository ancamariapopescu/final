import axios from 'axios'
const API = 'https://proiect-faza2-ancamariapopescu.c9users.io'

class TweetStore{
  constructor(ee){
    this.ee = ee
    this.content = []
  }
  getAll(id){
    axios(API + '/company/' + id + '/tweets')
      .then((response) => {
        this.content = response.data
        this.ee.emit('TWEET_LOAD')
      })
      .catch((error) => console.warn(error))
  }
  addOne(id, tweet){
    axios.post(API + '/company/' + id + '/tweets', tweet)
      .then(() => {
        this.ee.emit('TWEET_LOAD')})
      .catch((error) => console.warn(error))
  }
 
}

export default TweetStore
