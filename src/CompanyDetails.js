import React, {Component} from 'react'
import Tweet from './Tweet'
import TweetForm from './TweetForm'
import TweetStore from './TweetStore'
import {EventEmitter} from 'fbemitter'

const ee = new EventEmitter()
const store = new TweetStore(ee)

class CompanyDetails extends Component{
  constructor(props){
    super(props)
    this.state = {
      tweets : []
    }
    this.addTweet = (tweet) => {
      store.addOne(this.props.company.id, tweet)
    }
    this.deleteTweet = (tweet) => {
      store.deleteOne(this.props.company.id,tweet)
    }
    this.saveTweet = (tweet) => {
      store.saveOne(this.props.company.id,tweet)
      
    }
  }
  componentDidMount(){
    store.getAll(this.props.company.id)
    ee.addListener('TWEET_LOAD', () => {
      this.setState({
        tweets : store.content
      })
    })
  }
  render(){
    return (
      <div>
        <h2>Company: <b>{this.props.company.name}</b> </h2>
        <h4>{this.props.company.description}</h4>
        <h3>List of tweets from this company:</h3>
        {
          this.state.tweets.map((p) => <Tweet tweet={p} onDelete={this.deleteTweet} key={p.id} onSave={this.saveTweet} />)
        }
        <h3>Add another one.</h3>
        <TweetForm onAdd={this.addTweet}/>
      </div>  
    )
  }
}

export default CompanyDetails





