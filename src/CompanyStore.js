import axios from 'axios'
const API = 'https://proiect-faza2-ancamariapopescu.c9users.io'

class CompanyStore{
  constructor(ee){
    this.ee = ee
    this.content = []
    this.selected = null
  }
  getAll(){
    axios(API + '/get-all-companies')
      .then((response) => {
        this.content = response.data
        this.ee.emit('COMPANIES_LOAD')
      })
      .catch((error) => console.warn(error))
  }
  addOne(company){
    axios.post(API + '/register-company', company)
      .then(() => this.getAll())
      .catch((error) => console.warn(error))
  }
  deleteOne(id){
    axios.delete(API + '/company/' + id)
      .then(() => this.getAll())
      .catch((error) => console.warn(error))
  }
  saveOne(id, company){
    axios.put(API + '/company/' + id, company)
      .then(() => {
        this.ee.emit('COMPANIES_LOAD')
      })
      .catch((error) => console.warn(error))
  }
  getOne(id){
    axios(API + '/company/' + id)
      .then((response) => {
        this.selected = response.data
        this.ee.emit('SINGLE_COMPANY_LOAD')
      })
      .catch((error) => console.warn(error))
  }
}

export default CompanyStore
