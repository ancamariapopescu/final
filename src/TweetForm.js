import React,{Component} from 'react'

class TweetForm extends Component{
  constructor(props){
    super(props)
    this.state = {
      content : ''
    }
    this.handleChange = (event) => {
      this.setState({
        content : event.target.value
      })
    }
  }
  render(){
    return (<div>
      Content : <input type="text" onChange={this.handleChange}/>
     
      <input type="button" value="add" onClick={() => this.props.onAdd({content:this.state.content})} />
    </div>)
  }
}

export default TweetForm


