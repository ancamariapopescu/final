import React, { Component } from 'react';
import './App.css';
import {SignUp} from './SignUp';

import CompanyList from './CompanyList';

class App extends Component {
  
  constructor(props) {
    super(props);
    this.state = {
      nonMenu: true,
      menu:false
    };

    this.handleClick = this.handleClick.bind(this);
  }
  
  handleClick() {
    this.setState(state => ({
      menu:!state.menu,
      nonMenu:!state.nonMenu
    }));
    
  }

  render() {
    return (
     <div>
      <button id="switch" onClick={this.handleClick}>
        {this.state.signup ? 'Show menu' : 'Hide menu'}
      </button>
       {this.state.menu? <CompanyList /> : null}
       {this.state.nonMenu? <SignUp /> : null}
    </div>
    );
  }
}

export default App;
