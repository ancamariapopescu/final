import React from 'react';
import './SignUp.css'
import {Menu} from './Menu';

export class SignUp extends React.Component {
    
   constructor(props) {
    super(props);
    this.state = {
      showMenu:false
    };
     this.clickSignUp = this.clickSignUp.bind(this);
   }
   
    clickSignUp(){
    // alert('you sign up!');
     //salvare in baza de date
    //  this.setState(state => ({
    //   showMenu: !state.showMenu
    // }));
    // var a = document.getElementById('signup');
    // a.style.visibility = 'hidden';
    // var b = document.getElementById('switch');
    // b.style.visibility='hidden';
    }
    
     render(){
        return (
             <React.Fragment>
              <div>
              <div id="signup">
              <h1>SIGN UP</h1>
              <input type="email" id="email" placeholder="Email"/>
              <input type="text" id="first" placeholder="First Name"/>
              <input type="text" id="last" placeholder="Last Name"/>
              <input type="password" id="password" placeholder="Password"/>
              <input type="password" id="confirm" placeholder="Confirm Password"/>
              <br/>
                Type: <select id="select" name="type">
                 <option value="company">Company</option>
                 <option value="individual">Individual</option>
               </select>
            <input type="text" id="company_name" placeholder="Company Name"/>
            <input type="text" id="phone" placeholder="Phone number"/>
             <button id="send" onClick={this.clickSignUp}>Send</button>
            </div>
          
             {this.state.showMenu? <Menu/>: null}
             </div>
             </React.Fragment>
            )
         
     }
}