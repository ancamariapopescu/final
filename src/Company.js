import React,{Component} from 'react'

class Company extends Component{
  constructor(props){
    super(props)
    this.state = {
      isEditing : false,
      company : this.props.company,
      name : this.props.company.name
    }
    this.handleChange = (event) => {
      this.setState({
        name: event.target.value
      })
    }
  }
  componentWillReceiveProps(nextProps){
    this.setState({
      company : nextProps,
      name : this.props.company.name,
      isEditing : false
    })
  }
  render(){
  if(this.state.isEditing){
      return (<div>
       Company: <input type="text" name="name" value={this.state.name} onChange={this.handleChange}/>
      
        <input type="button" value="save" onClick={() => this.props.onSave(this.props.company.id, {name : this.state.name})}/>
        <input type="button" value="cancel" onClick={() => this.setState({isEditing : false})} />
      </div>)            
    }
    else{
      return (<div>
        Company: <b>{this.state.name}</b> 
             <input type="button" value="delete" onClick={() => this.props.onDelete(this.state.company.id)}/>
        <input type="button" value="edit" onClick={() => this.setState({isEditing : true})} />
        <input type="button" value="details" onClick={() => this.props.onSelect(this.props.company.id)}/> 
      </div>)
    }
  }
}

export default Company