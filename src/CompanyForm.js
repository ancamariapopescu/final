import React,{Component} from 'react'

class CompanyForm extends Component{
  constructor(props){
    super(props)
    this.state = {
      name : ''
    }
    this.handleChange = (event) => {
      this.setState({
        name : event.target.value
      })
    }
  }
  render(){
    return (<div>
      <h4>Companies:</h4>
    
      Name : <input type="text" name="companyName" onChange={this.handleChange}/>
       <input type="button" value="add" onClick={() => this.props.handleAdd({name : this.state.name})} />
    </div>)
  }
}

export default CompanyForm