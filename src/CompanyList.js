import React, {Component} from 'react'
import CompanyStore from './CompanyStore'
import {EventEmitter} from 'fbemitter'
import Company from './Company'
import CompanyForm from './CompanyForm'
import CompanyDetails from './CompanyDetails'

const ee = new EventEmitter()
const store = new CompanyStore(ee)

function addCompany(company){
  store.addOne(company)
}

function deleteCompany(id){
  store.deleteOne(id)
}

function saveCompany(id, company){
  store.saveOne(id, company)
}

class CompanyList extends Component{
  constructor(props){
    super(props)
    this.state = {
      companies : [],
      detailsFor : -1,
      selected : null
    }
    this.cancelSelection = () => {
      this.setState({
        detailsFor : -1
      })
    }
    this.selectCompany = (id) => {
      store.getOne(id)
      ee.addListener('SINGLE_COMPANY_LOAD', () => {
        this.setState({
          detailsFor : store.selected.id,
          selected : store.selected
        })
      })
    }
  }
  componentDidMount(){
    store.getAll()
    ee.addListener('COMPANIES_LOAD', () => {
      this.setState({
        companies : store.content
      })
    })
  }
  render(){
    if (this.state.detailsFor === -1){
      return (<div>
        {
          this.state.companies.map((c) => <Company company={c} onDelete={deleteCompany} key={c.id} onSave={saveCompany} onSelect={this.selectCompany} />)
        }
        <CompanyForm handleAdd={addCompany}/>
      </div>)      
    }
    else{
      return (
        <div>
          <CompanyDetails company={this.state.selected} />
          <input type="button" value="back" onClick={() => this.cancelSelection()}/>
        </div>  
      )
    }
  }
}

export default CompanyList



